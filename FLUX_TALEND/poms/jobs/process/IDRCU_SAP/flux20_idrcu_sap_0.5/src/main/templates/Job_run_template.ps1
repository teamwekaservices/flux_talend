$fileDir = Split-Path -Parent $MyInvocation.MyCommand.Path
cd $fileDir
java '-Xms256M' '-Xmx1024M' '-Dfile.encoding=UTF-8' -cp '.;../lib/routines.jar;../lib/dom4j-1.6.1.jar;../lib/jakarta-oro-2.0.8.jar;../lib/jsch-0.1.53.jar;../lib/log4j-1.2.16.jar;../lib/ojdbc7.jar;../lib/talendcsv.jar;flux20_idrcu_sap_0_5.jar;' rcu.flux20_idrcu_sap_0_5.FLUX20_IDRCU_SAP  %* 