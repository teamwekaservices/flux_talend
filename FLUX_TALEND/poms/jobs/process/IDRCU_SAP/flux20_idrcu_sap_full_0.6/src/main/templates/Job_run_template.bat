%~d0
cd %~dp0
java -Xms256M -Xmx1024M -cp .;../lib/routines.jar;../lib/dom4j-1.6.1.jar;../lib/jakarta-oro-2.0.8.jar;../lib/jsch-0.1.53.jar;../lib/log4j-1.2.16.jar;../lib/ojdbc7.jar;../lib/talendcsv.jar;flux20_idrcu_sap_full_0_6.jar; rcu.flux20_idrcu_sap_full_0_6.FLUX20_IDRCU_SAP_FULL  --context=Default %* 