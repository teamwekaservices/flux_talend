%~d0
cd %~dp0
java -Dtalend.component.manager.m2.repository=%cd%/../lib -Xms256M -Xmx1024M -cp .;../lib/routines.jar;../lib/activation.jar;../lib/dom4j-1.6.1.jar;../lib/log4j-1.2.17.jar;../lib/mail-1.4.jar;../lib/ojdbc7.jar;../lib/talendcsv.jar;flux_84_weka_sap_typologie_siret_0_2.jar; rcu.flux_84_weka_sap_typologie_siret_0_2.FLUX_84_Weka_SAP_typologie_SIRET  --context=PROD %*