$fileDir = Split-Path -Parent $MyInvocation.MyCommand.Path
cd $fileDir
java '-Dtalend.component.manager.m2.repository=%cd%/../lib' '-Xms256M' '-Xmx1024M' -cp '.;../lib/routines.jar;../lib/dom4j-1.6.1.jar;../lib/log4j-1.2.17.jar;../lib/mysql-connector-java-5.1.30-bin.jar;../lib/ojdbc7.jar;../lib/talendcsv.jar;statsrcucomundi_0_1.jar;' rcu.statsrcucomundi_0_1.StatsRCUComundi  --context=PROD %*